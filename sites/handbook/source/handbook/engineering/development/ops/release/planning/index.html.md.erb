---
layout: handbook-page-toc
title: 'Release Group Planning'
description: 'How the Release Team Operates'
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Our Approach

We work in a continuous Kanban manner while still aligning with Milestones. Epics and issues have the following lifecycle:

```mermaid
graph LR;
  Plan --> Schedule;
  Schedule --> Build;
  Build --> Release;
  Release --> Post-Release;
```

We use the following boards to monitor this process:

#### Planning Board

- [Release : Plan](https://gitlab.com/groups/gitlab-org/-/boards/1489550)

#### Scheduling Board

- [Release : Schedule](https://gitlab.com/groups/gitlab-org/-/boards/1488068?&label_name[]=group%3A%3Arelease&label_name[]=workflow%3A%3Ascheduling)

#### Build Board

- [Release : Build](https://gitlab.com/groups/gitlab-org/-/boards/1489558)

### Our Process

We use the [Planning Board](#planning-board) to triage issues that are not yet planned for a milestone. The Product Manager (PM) owns this board and uses it to advance issues through to becoming feasible Epics. When work leaves this board, it is in a fit state for the engineering team to implement the issue.

In this stage, the PM establishes new work that they would like to see in the Release features. The _devops::release_ process follows the [Product Development Flow](/handbook/product-development-flow/#validation-track), as follows:

#### Problem validation

The PM tries to establish that the issue describes a problem with the Release feature that is customer-relevant (internal and external),
or has some other clear merit (e.g. a technical improvement in the backend). The PM will draw on other group members, customers and the UX organization where necessary.
If the problem is validated, the issue is moved forward to the next stage, otherwise it is closed with a short explanation on _why_ the problem will not be considered. This explanation should also contain a link [to the category strategy](/handbook/product/product-processes/#category-direction).

#### Solution validation

Once the problem is validated, the PM will consider creating an Epic if [the scope of the solution is large enough](/handbook/product/product-processes/#epics-for-a-single-iteration).
They will then reach out to the Engineering Manager (EM) for a technical contact in the team. The EM assigns [ownership](#epic-ownership) to an engineer. The engineer will work with the PM to determine a technical solution for the problem.

If designs are required, the PM will add the `workflow::design` label and collaborate with UX to create the user stories required to solve the customer problem. Story mapping is a recommended way to visualize the user stories of a new feature in a holistic way. The story map can even be annotated to indicate which user stories need to be included in each release increment. More information on this process is noted in the [Product Development Workflow](/handbook/product-development-flow/#validation-phase-4-solution-validation). If PM and UX decide the issue is ready, they will move the issue to `workflow::planning breakdown`.

Occasionally, a proof-of-concept (POC) is necessary to determine a feasible technical path. When one is required, the PM
will create a POC issue that contains the context of the research to be conducted along with the goals of the POC. This
issue will be scheduled for work before any further breakdown of tasks is performed.

POC issues are also time-boxed with a due date applied to these items and should be labeled ~"POC". On the due date, the engineer is expected to provide a comment on the issue regarding the outcome of the POC. Time-boxing these issues is meant to constrain the scope of the work.

It is also important to note that not all POCs will be successful, and that is OK! Some avenues of research may not be successful, and the POC will have saved us from investing significant time in a solution that will not meet our needs. The goal is early feedback and fast iteration.

#### Planning Breakdown

The PM and the engineer will work together to create all of the issues required to build the solution. This could include

- documentation issues
- testing that is broader than the scope of individual issues
- post-release items such as rake tasks, migrations or monitoring issues

Post-release tasks that need to be performed by SRE's are created in the Infrastructure team project and cannot be
added to Epics as related issues. For these, it's useful to list them in the Epic description.

During planning breakdown, the correct labels should be applied. The required labels that need to be applied for each issue:

- `group::release`
- `category`
- `feature` or `bug`
- `frontend`, `backend` or both
- `needs weight`

This signifies that from both a product and engineering point of view, this issue is prepared and ready for weighting and scheduling.

##### Weights

As a byproduct of the engineering evaluation process, a rough estimate of the number of merge requests required to develop a
feature will be produced. This measure can be used as a way to determine issue weights. These weights can be useful during the
planning process to get a rough gauge of how many features can be worked on in a milestone, and to understand how much work the
team can do in a milestone. This metric also aligns with the merge request rate metric currently measured by engineering teams.

Every milestone a "Needs weight/Research" issue is created by the PM. The PM will work together with the EM to assign issues for weighting to each engineer on the team. These are issues that will likely be scheduled in the following 1+ milestones. During the first week of each milestone, the engineers will be responsible for weighing their assigned issues. 

After a weight has been entered, the engineer should remove the `needs weight` label, and add the `workflow::ready for development` label. This signals that the issue is ready to be scheduled in a future milestone.

#### Schedule

We use the [Scheduling boards](#scheduling-board) for this phase. Each milestone, the PM will create [a planning issue](https://gitlab.com/gitlab-org/ci-cd/release-group/release/-/blob/master/.gitlab/issue_templates/Internal%20Milestone.md)

The PM will populate the planning issue with the desired issues to be included in the milestone. These items will be grouped by priority labels to indicate product priorities: 

- `Release::P1`, which is the current milestone's top focus and will often have a `~Deliverable` label as well. 
- `Release::P2`, is the next milestone's top focus and will be assigned a `~Release::P1` in the following milestone.
- `Release::P3`, are future priorities that can be escalated into a sooner milestone.  

The EM will review the planning issue and determine if the team has enough capacity for the prioritized issues. During this process, the entire team is invited to review the milestone plan and give feedback asynchronosly. Once the plan is finalized, the EM will ensure all items have been assigned the appropriate milestone, which will pull items into the Build board.

Once per week, the PM and EM also meet to discuss the work that is currently active, and queue prepared items for development.
Some items that may be included on the agenda:

1. What is active now?
1. What needs to be active next?
1. Are there any deliverables?
1. Is the active list in priority order?
1. Bugs list
1. Technical debt items arising

#### Build

We use the [Build boards](#build-board) to look at issues in the current milestone labeled with `group::release`.

The Engineering Manager (EM) owns this board and uses it to facilitate building out the Epics and issues that the team have decided are ready for development.

Issues are added in the "ready for development" column in priority order. When an engineer is open, they can pick unassigned issues from the top of this list.

When picking up an issue, please:

1. Assign yourself
2. Adjust to the appropriate Milestone (_Will the issue be closed in the current or next Milestone_)

As their work progresses, they advance the issue through the "in dev" and "in review" columns. Engineers need to keep the issues aligned with the status of the corresponding merge requests.

When an issue is "in review", the MR should be assigned to both the original engineer and the reviewer so that it is clear that the merge request has an active reviewer. This helps the EM to see how much work is allocated to each person.

The column for "workflow::verification" is where the owner of the issue/epic, or the PM will ensure that the issue fits into the context of the solution. This is also where we confirm if a release note is required for this issue, and assign the correct milestone.

### Epic Ownership

The Release team uses epics to describe features or capabilities that will increase the maturity of the Release categories over time.

Each Epic should be owned by an engineer who is responsible for all technical aspects of that Epic. The engineering owner will work closely with the Product Manager to understand the requirements and create issues that encapsulate the technical work required. Each issue needs to be weighted and contain enough information for any other engineer on the team to be able to pick up that work.

**For the duration of building the epic**, the engineer does not need to be the only person implementing the issues. They should keep watch of the work that is done on the issues so that they can verify that the work is progressing correctly. If there are problems with the work, or lengthy delays,
they need to make sure the Product Manager and Engineering Manager are aware.

**When work is nearing completion**, the engineer should make sure that any additional issues that may have come up during the build process are either
addressed, or scheduled for work. This will help to make sure that we do not build up technical debt while building.

**Finally**, they should also monitor any work that needs to occur while rolling out the Epic in production. If there are
rake tasks, database migrations, or other tasks that need to be run, they need to see those through to being
run on the production systems with the help of the Site Reliability counterpart.

This places a lot of responsibility with the owner, but the PM and EM are always there to support them. This ownerships
removes bottlenecks and situations where only the PM or EM is able to advance an idea. In addition, the best people
to decide on how to implement an issue are often the people who will actually perform the work.

### Issue Breakdown

Sometimes an issue can be large and require multiple merge requests to resolve. In this case, it can help to create smaller, more narrowly defined issues that can be resolved with a single merge request.
Resolving these smaller implementation issues contributes to the completion of the larger parent issue. This has several benefits:

1. It keeps the conversations around each specific merge request within its own issue.
1. It makes it easier to see the progress of each merge request that contributes to the larger issue.
1. It promotes iteration and smaller merge requests.

For example, if an issue requires one frontend, one backend, and one database change, it can help to create three "sub-issues" that link back to the main issue.

To ensure the sequence for completion of the implementation issues is easy to track, we should utilize [`Blocks` and `Is blocked by` options](https://about.gitlab.com/handbook/marketing/project-management-guidelines/issues/#related-issues-and-blocking-issues) on each issue page.

![Related issues](/handbook/engineering/development/ops/release/planning/related-issues.png)

##### Addressing new issues quickly

When new issues arise (through testing, customer support issues, or other means) we still want to be able to address them
quickly without being delayed by process.
If you think a new issue needs to be worked on immediately:

1. Make sure there is enough detail on the ticket for someone else to understand the issue and for someone else to have
   enough context to review your work
1. Make sure there is a weight
1. Assign it into the current milestone
1. Add the `workflow::in dev` label and the `stuff that should just work` label
1. Assign it to yourself

##### What work should I pick up next?

"Pull from the right". That means, start at the right hand side of the build board, and move towards the left.

In order, that means:

- Can you help anyone with reviews?
- Can you unblock anyone in development?
- Pick from the "ready for development" column using the "Release::P*" labels on the [Build boards](#build-board).
